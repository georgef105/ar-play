import {
  Controllers,
  Interactive,
  XR,
  XRButton,
  useHitTest,
} from "@react-three/xr";
import React, {
  ComponentProps,
  ComponentType,
  FC,
  useRef,
  useState,
} from "react";
import {Cylinder, Ring} from "@react-three/drei";
import {Canvas, Vector3} from "@react-three/fiber";

type ARViewProps = {};

export const ARView: ComponentType<ARViewProps> = () => {
  const [error, setError] = useState<string>();
  const [count, setCount] = useState<number>(0);

  const pinsRef = useRef<ComponentProps<typeof MarkerPoint>[]>([]);

  const onScopeSelected = (position: Vector3) => {
    setCount(count + Math.round(Math.random() * 10) / 10);
    pinsRef.current.push({
      position,
      color: "blue",
    });
  };
  return (
    <>
      {error && <div>ERROR: {error}</div>}
      <XRButton
        mode="AR"
        sessionInit={{
          requiredFeatures: ["hit-test", "local-floor"],
        }}
        onError={(error) => setError(error.message)}
      />
      <Canvas>
        <XR referenceSpace="local-floor">
          <ambientLight intensity={0.5} />
          <pointLight position={[5, 5, 5]} />
          <Controllers />
          <MarkerPoint position={[0, 0, 0]} color="red" />
          <GroundScope onSelect={(position) => onScopeSelected(position)} />
          {pinsRef.current.map((pin, index) => (
            <MarkerPoint key={index} {...pin} />
          ))}
        </XR>
      </Canvas>
    </>
  );
};

const MarkerPoint: FC<{position: Vector3; color: string}> = ({
  position,
  color,
}) => {
  return (
    <Cylinder args={[0.05, 0.05, 0.01]} position={position}>
      <meshBasicMaterial color={color} />
    </Cylinder>
  );
};

const GroundScope: FC<{onSelect: (position: Vector3) => void}> = ({
  onSelect,
}) => {
  const scopeRef = useRef<THREE.Mesh>(null!);

  useHitTest((hitMatrix) => {
    hitMatrix.decompose(
      scopeRef.current.position,
      scopeRef.current.rotation as any,
      scopeRef.current.scale
    );
  });

  return (
    <Interactive onSelect={() => onSelect(scopeRef.current.position)}>
      <Ring ref={scopeRef} args={[0.05, 0.1]}>
        <meshBasicMaterial color="hotpink" />
      </Ring>
    </Interactive>
  );
};
